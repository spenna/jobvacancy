# Helper methods defined here can be accessed in any controller or view in the application

JobVacancy::App.helpers do
  def job_offer_params
    params[:job_offer_form].to_h.symbolize_keys
  end

  def job_offer_required_experience(job_offer)
    required_experience = job_offer.required_experience
    return 'Not specified' if required_experience.nil? || required_experience.zero?

    required_experience
  end
end
