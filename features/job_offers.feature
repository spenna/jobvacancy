Feature: Job Offers

  Scenario: See job offers with required experience
    Given A job offer with required experience offer exists in the offers list
    When I browse the job offers page
    Then The job offer should have the required experience

  Scenario: See job offers with no required experience
    Given A job offer with no required experience offer exists in the offers list
    When I browse the job offers page
    Then The job offer should have the required experience as not specified
