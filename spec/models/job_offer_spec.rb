require 'spec_helper'

describe JobOffer do
  subject(:job_offer) { described_class.new(title: 'Title') }

  describe 'model' do
    it { is_expected.to respond_to(:required_experience) }
  end

  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end

    it 'should be invalid when required experience is negative' do
      validation_msg = 'Required experience must be greater than or equal to 0'
      check_validation(:required_experience, validation_msg) do
        described_class.new(title: 'a title', required_experience: -2)
      end
    end
  end
end
